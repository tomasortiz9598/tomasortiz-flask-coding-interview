from datetime import date
import json

DATA_FOLDER = "./data/"
USERS_FILE = "users.json"
CATEGORIES_FILE = "categories.json"
PRODUCTS_FILE = "products.json"


def route(file):
    return DATA_FOLDER + file


def data_source(filename):
    with open(route(filename)) as file:
        return json.load(file)


def age(birth_date):
    today = date.today()
    return today.year - birth_date.year - ((today.month, today.day) < (birth_date.month,  birth_date.day))


def update_age(user):
    return user.update(age=age(date.fromisoformat(user["birth_date"])))


def products_count(category_id):
    categories = data_source(PRODUCTS_FILE)
    return len(list(filter(lambda x: x["category_id"] == category_id, categories)))
