from utils import USERS_FILE, CATEGORIES_FILE
import utils


def users():
    return utils.data_source(USERS_FILE)


def categories():
    categories = utils.data_source(CATEGORIES_FILE)
    print(categories)
    return list(map(lambda x: x.update(count=utils.products_count(x["id"])), categories))