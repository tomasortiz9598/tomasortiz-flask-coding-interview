from marshmallow import Schema, fields, post_load
from utils import route, USERS_FILE
import json
import utils
class User:
    '''
    {"id": 1, "first_name": "Angeli", "last_name": "Flaxon", "email": "aflaxon0@bandcamp.com", "gender": "Male",
          "ip_address": "208.143.161.251", "birth_date": "1991-01-15"}
    '''
    def __init__(self, id, first_name, last_name, email, gender, ip_address, birth_date):
        self.birth_date = birth_date
        self.ip_address = ip_address
        self.gender = gender
        self.email = email
        self.last_name = last_name
        self.first_name = first_name
        self.id = id


class UserSchema(Schema):
    id = fields.Int()
    first_name = fields.String()
    last_name = fields.String()
    email = fields.Email(required=True)
    gender = fields.String(required=True)
    ip_address = fields.String()
    birth_date = fields.DateTime(required=True)

    @post_load
    def make_user(self, data, **kwargs):
        return User(**data)



def load_users():
    users_data = utils.data_source(USERS_FILE)
    map(utils.update_age, users_data)
    UserSchema(many=True).load(users_data)
