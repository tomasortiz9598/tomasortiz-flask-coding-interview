from flask import Flask
import logic
app = Flask(__name__)


@app.route("/")
def main():
    response = {
        'message': 'Welcome to the BEON Python/Django Challenge'
    }
    return response, 200


@app.route("/users")
def users():
    return logic.users(), 200


@app.route("/categories")
def categories():
    return logic.categories(), 200


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, debug=True)
